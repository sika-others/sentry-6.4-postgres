DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'sentry',
        'USER': 'sentry',
        'PASSWORD': 'sentry',
        'HOST': 'localhost',
        'PORT': '5432',
        'OPTIONS': {
            'autocommit': True,
        }
    }
}

SENTRY_URL_PREFIX = 'http://sentry'

DEBUG=True
SENTRY_WEB_HOST = '127.0.0.1'
SENTRY_WEB_PORT = 1349
SENTRY_WEB_OPTIONS = {
    'workers': 3,
    'secure_scheme_headers': {'X-FORWARDED-PROTO': 'https'},
}

