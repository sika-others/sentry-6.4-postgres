#!/bin/bash

PACKAGE=sentry-6.4-postgres.deb

[ -d build ] || mkdir build

dpkg-deb --build src $PACKAGE

mv $PACKAGE build
